import java.util.*;
import java.io.*;

class Cluster{

	public List<String[]> points;
	public String[] centroid;
	public int id;

	public Cluster(int id){
		this.id = id;
		this.points = new ArrayList<String[]>();
		this.centroid = null; 
	} 
}

public class k_means {

	private static int num_clusters = 0;
	private static int num_points = 0;

	private static int cusId = 0;

	private static int min = 0;
	private static int max = 0;

	private static List <Cluster> clusters = new ArrayList<Cluster>();

	private static String[] product;

	private static List<String[]> training = new ArrayList<String[]>();

	public k_means() {
		
	}

		public static void main(String[] args) {
		String tmp = args[1];
		num_clusters = Integer.parseInt(tmp);

		try {
			readFile(args[0]);
		} catch (FileNotFoundException e) {
			System.out.println("File not found");
		}

		init();
		calculate();
	}

	public static void readFile(String training_) throws FileNotFoundException {
		File infile = new File(training_);
	    String line = "";

	    try {
	       Scanner scan = new Scanner(infile);

	       line = scan.nextLine();
	       String p[] = line.split(",");
	       product = p;
	      
	       num_points = product.length;

	       while(scan.hasNext()) {
	       	line = scan.nextLine();
	      	String[] str = line.split(",");		    
	        training.add(str);
	       }

	    } catch(FileNotFoundException e) {
	      System.out.println("file not found");
	    }
	}

	public static String[][] defineMaxNMin(int size) {

		String[][] result = new String[2][size-2]; // 0 is min & 1 is max
		double max = 0.0;
		double min = 0.0;

		String[] max_min = training.get(0);
		for (int i = 1; i < size-1; i++) {
			max = Double.parseDouble(max_min[i]);
			min = Double.parseDouble(max_min[i]);
			for (int j = 1; j < training.size(); j++) {
				String[] tmp = training.get(j);
				double temp = Double.parseDouble(tmp[i]);
			
				if (temp > max) {
					max = temp;
				}
				if (temp < min) {
					min = temp;
				}
			}
			result[0][i-1] = String.valueOf(min);
			result[1][i-1] = String.valueOf(max);		
		}

		return result;
	}

	public static String[] createRandomCentroid(String[][] point, int size) {
		Random r = new Random();
		String[] centroid = new String[size];
		centroid[0] = "centroid " + cusId;
		cusId++;
		for (int i = 0; i < point[0].length; i++) {
			double min = Double.parseDouble(point[0][i]);
			double max = Double.parseDouble(point[1][i]);
			double tmp = min + (max - min) * r.nextDouble();

			centroid[i+1] = String.valueOf(tmp);
		}
		centroid[num_points - 1] = "0";
	
		return centroid;
	}	


	public static void init() {
		String[][] tmp = defineMaxNMin(num_points);
		for(int i = 0; i < num_clusters; i++) {
			Cluster cluster = new Cluster(i);
			String[] centroid = createRandomCentroid(tmp, num_points);
			cluster.centroid = centroid;
			clusters.add(cluster);
		}
	}

	public static void calculate() {
		boolean finish = false;
		int iteration = 0;

		while(!finish) {


			clearClusters();

			List<String[]> lastCentroids = getCentroids();

			assignCluster();

			calculateCentroids();

			iteration++;

			List<String[]> currentCentroids = getCentroids();

			double distance = 0.0;
			for (int i = 0; i < lastCentroids.size(); i++) {

				distance += distance(lastCentroids.get(i), currentCentroids.get(i));

			}
			if (distance == 0) {
				finish = true;
				System.out.println("#######################");
				System.out.println("iteration: " + (iteration+1));
				System.out.println("centroid distances: " + distance);
				printResult();
			}
		}
	}

	public static void printResult() {

		for (int i = 0; i < num_clusters; i++) {
			Cluster c = clusters.get(i);

			System.out.println("[Cluster: " + c.id + "]");
			System.out.println("[Centroid: " + c.centroid[0] + "]");
			System.out.println("[Points: \n" );
			
			for (String[] p : c.points) {
				System.out.print(p[0] + ", ");	
			}
			
			System.out.println("]");
		}

	}

	private static void clearClusters() {
		for(Cluster cluster : clusters){
			cluster.points.clear();
		}
	}

	private static double distance(String[] point, String[] centroid) {
		int result = 0;
		for (int i = 1; i < point.length-1; i++) {
			double c = Double.parseDouble(centroid[i]);
			double p = Double.parseDouble(point[i]);
			result += Math.pow((c - p),2);
		}
		return Math.sqrt(result);
	}

	private static List<String[]> getCentroids() {
		List<String[]> centroids = new ArrayList<String[]>(num_clusters);
		for(Cluster cluster : clusters) {
			String[] aux = cluster.centroid;
			centroids.add(aux);
		}
		return centroids;
	}

	private static void assignCluster() {
		double max = Double.MAX_VALUE;
		double min = max;
		int cluster = 0;
		double distance = 0.0;

		for(String[] point : training) {
			min = max;
			for (int i = 0; i < num_clusters; i++) {
				Cluster c = clusters.get(i);
				distance = distance(point, c.centroid);
				if (distance < min) {
					min = distance;
					cluster = i;
				}
			}

			point[num_points - 1] = String.valueOf(cluster);
			clusters.get(cluster).points.add(point);
		}
	}

	private static void calculateCentroids() {
		for(Cluster cluster : clusters) {
			String[] newC = new String[num_points];
			List<String[]> list = cluster.points;
			int n_points = list.size();
			for(String[] point : list) {
				for(int i = 1; i < num_points-1; i++) {
					newC[i] = point[i];	
				}
			}

			String[] centroid = cluster.centroid;
			if (n_points > 0) {
				for (int i = 1; i < num_points-1; i++) {
					double tmp = Double.parseDouble(newC[i]);
					double result = tmp / n_points;
					centroid[i] = String.valueOf(result);
				}
			}
		}
	}
}