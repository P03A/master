import pandas as pd
import numpy as np
import matplotlib.pylab as plt

outputplot = pd.read_csv('/Users/paulyuzhu/Desktop/QQQQ/output.csv', dtype=np.float32, header=None)

new_out = outputplot.as_matrix()
output=np.array(new_out)
print(output.shape)


output_array=np.empty(shape=(20,83))

for i in range(4):
    output_array[i :]=np.sum(output[i*10 : i*10+10],axis=0)
    
    
print(output_array)


plt.hist(output_array.ravel(), bins=np.linspace(np.min(output_array), np.max(output_array))) 
plt.xlabel("number of products customers buy")
plt.ylabel("number of people who buy this amount of products")
plt.title("output distribution")

plt.show()


inputplot = pd.read_csv('/Users/paulyuzhu/Desktop/product.csv', usecols = list(range(1,84)),dtype=np.float32,header = 0)

new_in = inputplot.as_matrix()
input=np.array(new_in)
print(input.shape)
print(input)

input_array=np.empty(shape=(7,83))

for i in range(5):
    input_array[i :]=np.sum(input[i*100 : i*100+100],axis=0)
    
input_array[6 :]=np.sum(input[599:612],axis=0)
    
print(input_array)


plt.hist(input_array.ravel(), bins=np.linspace(np.min(input_array), np.max(input_array))) 
plt.xlabel("number of products customers buy")
plt.ylabel("number of people who buy this amount of products")
plt.title("input distribution")

plt.show()