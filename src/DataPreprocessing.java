import java.util.*;
import java.io.*;
import java.nio.charset.Charset;

import com.csvreader.CsvWriter;

public class DataPreprocessing {
	
	public static ArrayList<String> name = new ArrayList<String>();
	public static ArrayList<String> quantity = new ArrayList<String>();
	public static ArrayList<String> customer = new ArrayList<String>();

	public static void main(String[] args) {
		CsvWriter csvWriter = CSV();
		int size = read("customer.txt");
		int n = readProduct("product.txt");
		System.out.print(name.size());
		String[] arrString = (String[]) name
				.toArray(new String[name.size()]);
		write(csvWriter, arrString);
		
		combine("info.txt",csvWriter);
		 
		

		csvWriter.close();

	}

	public static CsvWriter CSV() {

		String filePath = "product.csv";

		
		CsvWriter csvWriter = new CsvWriter(filePath, ',',
				Charset.forName("GBK"));
		// CsvWriter csvWriter = new CsvWriter(filePath);
		return csvWriter;

		

	}

	public static void write(CsvWriter csvWriter, String[] content) {
		try {
			csvWriter.writeRecord(content);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static int read(String filename) {
		File file = new File(filename);

		//ArrayList<String> name = new ArrayList<String>();
	
		String[] arr;
		BufferedReader reader = null;
		int num=0;
		try {
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			int line = 1;
					
			while ((tempString = reader.readLine()) != null) {
				// 显示行号
                num++;
				
                if(!customer.contains(tempString)){
                	 customer.add(tempString);
                }
           
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
		return num;
		

	}
	public static int combine(String filename,CsvWriter csvWriter) {
		File file = new File(filename);

		String[] arr;
		//BufferedReader reader = null;
		int num=0;
	
			int line = 1;
			// 一次读入一行，直到读入null为文件结束
		
			 for(int i=0;i<customer.size();i++){
					ArrayList<String> info = new ArrayList<String>();
					info.add(customer.get(i));
					BufferedReader reader = null;
					try {
						// System.out.println("以行为单位读取文件内容，一次读一整行：");
						reader = new BufferedReader(new FileReader(file));
						String tempString = null;
			while ((tempString = reader.readLine()) != null) {
				// 显示行号

                num++;
               
				arr = tempString.split("	");
				if(arr.length>2){
            	   if(customer.get(i).equals(arr[2].trim())){
            		   for(int j=1;j<name.size();j++){
            			   if(name.get(j).equals(arr[0])){
            				   if(info.size()<=j){
                				   info.add(j, arr[1]);
                				   }else{
                					   info.set(j, arr[1]);
                				   }
            			   }else{
            				   if(info.size()<=j){
            				   info.add(j, "0");
            				   }
            			   }
            		   }
            	   }
               }
			}
			String[] arrString = (String[]) info
					.toArray(new String[info.size()]);
			write(csvWriter, arrString);
  
			
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
			 }
		return num;

	}
	public static int readProduct(String filename) {
		File file = new File(filename);

		//ArrayList<String> name = new ArrayList<String>();
	
		String[] arr;
		BufferedReader reader = null;
		name.add(" ");
		int num=0;
		try {
			// System.out.println("以行为单位读取文件内容，一次读一整行：");
			reader = new BufferedReader(new FileReader(file));
			String tempString = null;
			int line = 1;
			// 一次读入一行，直到读入null为文件结束
		
			while ((tempString = reader.readLine()) != null) {
				// 显示行号
                num++;
				//arr = tempString.split("	");
				 if(!name.contains(tempString)){
                	 name.add(tempString);
                }
			
		
			}
			reader.close();
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (reader != null) {
				try {
					reader.close();
				} catch (IOException e1) {
				}
			}
		}
		return num;


	}
	

	

}


