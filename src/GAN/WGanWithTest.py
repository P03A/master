#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 13:45:18 2017

@author: franklan123
"""

import tensorflow as tf
import numpy as np
import pandas as pd
import csv
import os
import matplotlib.pyplot as plt

tf.reset_default_graph()
#batch size
mb_size = 32
#column of data
X_dim = 83

#row of data
z_dim = 613
#hidden units in one layer
h_dim = 128

# number ooutput
number_output = 200

# read csv file
df = pd.read_csv('/Users/franklan123/Desktop/product.csv', usecols = list(range(1,X_dim+1)),dtype=np.float32,header = 0)
d = df.as_matrix()
# normalize the data
rmean = np.nanmean(d)
ee= df.max().as_matrix()
zmax = np.amax(ee)
smin = np.amin(ee)
rrange = zmax - smin
data = (d - rmean)/rrange

# get max,min,max-min in every column
column_max = []
column_min = []
column_range = []

for i in range (X_dim):     
        column_max.append(np.max(d[:,i]))    
        column_min.append(np.min(d[:,i]))
      
for i in range(X_dim):
    column_range.append(column_max[i] - column_min[i])
      
# unpack the data
g = tf.unstack(data, num = z_dim, axis = 0)


x=np.array(d)
for i in range(z_dim):
    for j in range(X_dim):
        if column_range[j] == 0:
            x[i][j] = 0
        else:
            x[i][j]=(x[i][j] - column_min[j])/column_range[j]

# generate random data
def xavier_init(size):
    in_dim = size[0]
    xavier_stddev = 1. / tf.sqrt(in_dim / 2.)
    return tf.random_normal(shape=size, stddev=xavier_stddev)


X = tf.placeholder(tf.float32, shape=[None, X_dim])

D_W1 = tf.Variable(xavier_init([X_dim, h_dim]))
D_b1 = tf.Variable(tf.zeros(shape=[h_dim]))

D_W2 = tf.Variable(xavier_init([h_dim, 1]))
D_b2 = tf.Variable(tf.zeros(shape=[1]))

theta_D = [D_W1, D_W2, D_b1, D_b2]


z = tf.placeholder(tf.float32, shape=[None, z_dim])

G_W1 = tf.Variable(xavier_init([z_dim, h_dim]))
G_b1 = tf.Variable(tf.zeros(shape=[h_dim]))

G_W2 = tf.Variable(xavier_init([h_dim, X_dim]))
G_b2 = tf.Variable(tf.zeros(shape=[X_dim]))

theta_G = [G_W1, G_W2, G_b1, G_b2]


def sample_z(m, n):
    return np.random.uniform(-1., 1., size=[m, n])

# generator model
def generator(z):
    G_h1 = tf.nn.relu(tf.matmul(z, G_W1) + G_b1)
    G_log_prob = tf.matmul(G_h1, G_W2) + G_b2
    G_prob = tf.nn.sigmoid(G_log_prob)
    return G_prob

# discriminator model
def discriminator(x):
    D_h1 = tf.nn.relu(tf.matmul(x, D_W1) + D_b1)
    out = tf.matmul(D_h1, D_W2) + D_b2
    return out

G_sample = generator(z)
D_real = discriminator(X)
D_fake = discriminator(G_sample)

with tf.name_scope('dloss'):
    D_loss = tf.reduce_mean(D_real) - tf.reduce_mean(D_fake)

with tf.name_scope('gloss'):
    G_loss = -tf.reduce_mean(D_fake)

D_solver = (tf.train.RMSPropOptimizer(learning_rate=1e-4)
            .minimize(-D_loss, var_list=theta_D))
G_solver = (tf.train.RMSPropOptimizer(learning_rate=1e-4)
            .minimize(G_loss, var_list=theta_G))

clip_D = [p.assign(tf.clip_by_value(p, -0.01, 0.01)) for p in theta_D]

tf.summary.scalar("G_loss", G_loss)

X_mb, *_ = tf.train.batch(g ,mb_size, capacity = z_dim)

# draw Gloss Dloss graph in tensorflow
summary_op = tf.summary.merge_all()

sess = tf.train.MonitoredSession()

if not os.path.exists('/Users/franklan123/Desktop/QQQQ'):
    os.makedirs('/Users/franklan123/Desktop/QQQQ')

# put the data into tensorboard logfile 
log_writer = tf.summary.FileWriter('/Users/franklan123/Desktop/logfile',graph = tf.get_default_graph())
i = 0

# training part
for it in range(4001):  
    for _ in range(5):    
        x_batch = sess.run(X_mb)     
        _, D_loss_curr, _ = sess.run(               
            [D_solver, D_loss, clip_D],
            feed_dict={X: x_batch, z: sample_z(mb_size, z_dim)}
        )
    _, G_loss_curr = sess.run(
        [G_solver, summary_op],
        feed_dict={z: sample_z(mb_size, z_dim)}
    )
    
    log_writer.add_summary(G_loss_curr, it)
    
    if it % 100 == 0:
       
        print(it)

        if it == 4000:
          
            samples = sess.run(G_sample, feed_dict={z: sample_z(number_output, z_dim)})
            
            #draw output distribution
            plt.hist(samples.ravel(), bins=np.linspace(np.min(samples), np.max(samples)))
            plt.xlabel("probability")
            plt.ylabel("number of datapoints")
            plt.title("output distribution")
            plt.show()
          
            result = [[0 for x in range (X_dim)] for y in range(number_output)]
            for i in range (number_output):
                for j in range (X_dim):
                    samples[i][j] = samples[i][j] * column_range[j]+column_min[j]                  
                    result[i][j] =  int(round(np.asscalar(samples[i][j]),0))
                    
            for i in range (number_output):
                result[i].insert(0,i + 1)
            with open('/Users/franklan123/Desktop/QQQQ/output.csv', 'w', newline = '') as f:
                writer = csv.writer(f)
                writer.writerows(['1'])
                writer.writerows(result)
                                     
            i += 1

# draw input distribution
plt.hist(x.ravel(), bins=np.linspace(np.min(x), np.max(x)))
plt.xlabel("probability")
plt.ylabel("number of datapoints")
plt.title("input distribution")
plt.show()
