#How to run GAN program    
###GAN

(1) open anaconda    
(2) change to tensorflow channels and open spyder,copy wganwithtest.py and paste into spyder,download product.csv (It is in the Bitbucket Download)    
(3) line31, change input data directory     
(4) line136,137, create a folder for output data    
(5) line 140, create a log file for tensorboard    
(6) line181, change output data directory    
(7) After you click run button, you will see a series of training time numbers(0, 100, 200…. 4000)     
(8) After program ends, you will see two probablity distribution test graph.    
(9) open your command line tool    
(10) type command:    source activate tensorflow    
(10) type command:     tensorboard —logdir=＝‘your log file directory’, after that you will get a URL    
(11) copy URL, and paste to your browser, you will see a test graph.    
(12) open your output file, then copy the first row of input cvs and paste to the first row of output csv.    



###Clustering
* java k-means xxx.csv #numOfK    
###Visualisation
* turn on localhost server (XAMPP recommended)
* host homepage.html
* open in browser


